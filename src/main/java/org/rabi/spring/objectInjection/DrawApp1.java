package org.rabi.spring.objectInjection;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DrawApp1 {
    public static void main(String arg[])
    {
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        Triangle1 triangle1 = (Triangle1) applicationContext.getBean("triangle1");
        triangle1.draw();
    }
}
