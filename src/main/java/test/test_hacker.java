package test;

public class test_hacker {
    // Complete the counts function below.
    static int[] counts(int[] nums, int[] maxes) {
        int result[]=new int[maxes.length];
        int n  = 0;
        for (int i:maxes)
        {
            int largerst=0;
            for (int j:nums)
            {
                if (i<=j)
                    largerst++;
            }
            result[n]=largerst;
            n++;
        }
        return result;
    }
    public static void main(String arg[])
    {
        test_hacker testHacker=new test_hacker();
       // int[] tt=new int[]{23,4,2,12,18,20,16};
        //int[] tt1=new int[]{13,22,29,1};
        int[] tt=new int[]{1,4,2,4};
        int[] tt1=new int[]{3,5,3};
        counts(tt,tt1);
    }
}
