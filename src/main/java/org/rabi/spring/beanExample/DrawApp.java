package org.rabi.spring.beanExample;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DrawApp {
    public static void main(String arg[])
    {
        //BeanFactory factory=new XmlBeanFactory(new FileSystemResource("old_spring.xml")); //bean factory
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("old_spring.xml");
        Triangle triangle= (Triangle) applicationContext.getBean("triangle");
        //Triangle1 triangle=new Triangle1();  without bean
        triangle.draw();
    }
}
