package org.rabi.spring.beanExample;

public class Triangle {

    public Triangle(String type)
    {
        this.type=type;
    }
    public Triangle(String type,int height)
    {
        this.type=type;
        this.height=height;
    }
    public Triangle(int height)
    {
        this.height=height;
    }
    private String type;

    public int getHeight() {
        return height;
    }

    private int height;
    public String getType() {
        return type;
    }

    /*public void setHeight(int type) {
        this.height = type;
    }
    public void setType(String type) {
        this.type = type;
    }*/
    public void draw() {
        System.out.println("draw the triangle "+getType()+" whose height is "+getHeight());
    }
}
